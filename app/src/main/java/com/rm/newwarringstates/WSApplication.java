package com.rm.newwarringstates;

import android.app.Application;

import com.rm.newwarringstates.utils.database.DatabaseUtils;

/**
 * Created by User on 2018/1/11.
 */

public class WSApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseUtils.init();
    }
}
