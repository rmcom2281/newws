package com.rm.newwarringstates.object;

/**
 * Created by User on 2017/12/30.
 */

public class Player {
    private int id;
    private int name;

    private int exp = 0;
    private int follower = 0;
    private int food = 1000;
    private int gold = 1000;
    private int cinnabar = 0;
    private int horse = 1000;

    private int lv_player = 1;
    private int lv_hall = 1;
    private int lv_food = 1;
    private int lv_follower = 1;
    private int lv_book = 0;
    private int lv_horse = 1;
    private int lv_alchemy = 0;
    private int lv_blacksmith = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getFollower() {
        return follower;
    }

    public void setFollower(int follower) {
        this.follower = follower;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getCinnabar() {
        return cinnabar;
    }

    public void setCinnabar(int cinnabar) {
        this.cinnabar = cinnabar;
    }

    public int getHorse() {
        return horse;
    }

    public void setHorse(int horse) {
        this.horse = horse;
    }

    public int getLv_player() {
        return lv_player;
    }

    public void setLv_player(int lv_player) {
        this.lv_player = lv_player;
    }

    public int getLv_hall() {
        return lv_hall;
    }

    public void setLv_hall(int lv_hall) {
        this.lv_hall = lv_hall;
    }

    public int getLv_food() {
        return lv_food;
    }

    public void setLv_food(int lv_food) {
        this.lv_food = lv_food;
    }

    public int getLv_follower() {
        return lv_follower;
    }

    public void setLv_follower(int lv_follower) {
        this.lv_follower = lv_follower;
    }

    public int getLv_book() {
        return lv_book;
    }

    public void setLv_book(int lv_book) {
        this.lv_book = lv_book;
    }

    public int getLv_horse() {
        return lv_horse;
    }

    public void setLv_horse(int lv_horse) {
        this.lv_horse = lv_horse;
    }

    public int getLv_alchemy() {
        return lv_alchemy;
    }

    public void setLv_alchemy(int lv_alchemy) {
        this.lv_alchemy = lv_alchemy;
    }

    public int getLv_blacksmith() {
        return lv_blacksmith;
    }

    public void setLv_blacksmith(int lv_blacksmith) {
        this.lv_blacksmith = lv_blacksmith;
    }
}
