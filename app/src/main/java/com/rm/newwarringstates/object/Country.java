package com.rm.newwarringstates.object;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2017/12/30.
 */

public class Country {

    private String name;
    private int id;
    private int power = 500;
    private List<City> cities = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
