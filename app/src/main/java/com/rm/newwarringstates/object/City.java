package com.rm.newwarringstates.object;

/**
 * Created by User on 2017/12/30.
 */

public class City {

    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
