package com.rm.newwarringstates.object.database;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

/**
 * Created by caia on 2018/1/11.
 */

@IgnoreExtraProperties
public class TestTask {
    public int id;
    public int needTime;
    public Object now;

    public TestTask(){
    }

    public TestTask(int id, int needTime, Object now) {
        this.id = id;
        this.needTime = needTime;
        this.now = now;
    }
}
