package com.rm.newwarringstates.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.FollowerAdapter;
import com.rm.newwarringstates.data.Constants;
import com.rm.newwarringstates.object.Follower;
import com.rm.newwarringstates.utils.DisplayUtils;
import com.rm.newwarringstates.utils.FollowerUtils;

import java.util.List;

/**
 * Created by caia on 2018/1/2.
 */

public class FollowerFragment extends DialogFragment{

    private int mPlayerId;
    private View mView;
    private RecyclerView mRecyclerView;
    private FollowerAdapter mAdapter;
    private List<Follower> mDataList;

    public static FollowerFragment newInstance(int playerId){
        FollowerFragment fragment = new FollowerFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARGS_NAME_PLAYER_ID, playerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPlayerId = getArguments().getInt(Constants.ARGS_NAME_PLAYER_ID);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mView = inflater.inflate(R.layout.fragment_follower, container, false);
        mRecyclerView = mView.findViewById(R.id.person_recycler_view);
        mAdapter = new FollowerAdapter(getActivity(), new FollowerAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {
                if(position==0){
                    Toast.makeText(getActivity(), "add new follower", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "u  pressed "+mDataList.get(position-1).getName(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mView.setPadding(0, 0, 0, 0);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDataList = FollowerUtils.generateNewFollower(10);
        mAdapter.setData(mDataList);
        if(mView!=null){
            mView.getLayoutParams().width = DisplayUtils.getScreenWidth(getActivity());
        }
    }
}
