package com.rm.newwarringstates.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.home.HallMessageAdapter;
import com.rm.newwarringstates.adapter.home.HallQuestAdapter;
import com.rm.newwarringstates.fragment.HomeFragment;
import com.rm.newwarringstates.object.home.Message;
import com.rm.newwarringstates.object.home.Quest;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by caia on 2018/1/17.
 */

public class HallFragment extends HomeFragment implements View.OnClickListener {
    private GifImageView mBackground;

    private List<Quest> mMainDataList;
    private List<Quest> mDailyDataList;
    private List<Message> mMessageDataList;

    private HallQuestAdapter mMainAdapter;
    private HallQuestAdapter mDailyAdapter;
    private HallMessageAdapter mMessageAdapter;

    private RecyclerView mMainRecyclerView;
    private RecyclerView mDailyRecyclerView;
    private RecyclerView mMessageRecyclerView;

    private Button mMainBtn;
    private Button mDailyBtn;
    private Button mMessageBtn;

    private int mListType = 0;
    private int LIST_MAIN_QUEST = 0;
    private int LIST_DAILY_QUEST = 1;
    private int LIST_MESSAGE = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_hall, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        mMainRecyclerView = rootView.findViewById(R.id.recycler_home_hall_quest_main);
        mMainAdapter = new HallQuestAdapter(getActivity(), new HallQuestAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        mMainRecyclerView.setLayoutManager(linearLayoutManager);
        mMainRecyclerView.setAdapter(mMainAdapter);

        mDailyRecyclerView = rootView.findViewById(R.id.recycler_home_hall_quest_daily);
        mDailyAdapter = new HallQuestAdapter(getActivity(), new HallQuestAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        final LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(LinearLayout.VERTICAL);
        mDailyRecyclerView.setLayoutManager(linearLayoutManager2);
        mDailyRecyclerView.setAdapter(mDailyAdapter);

        final LinearLayoutManager linearLayoutManager3 = new LinearLayoutManager(getActivity());
        linearLayoutManager3.setOrientation(LinearLayout.VERTICAL);
        mMessageRecyclerView = rootView.findViewById(R.id.recycler_home_hall_message);
        mMessageAdapter = new HallMessageAdapter(getActivity(), new HallMessageAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        mMessageRecyclerView.setLayoutManager(linearLayoutManager3);
        mMessageRecyclerView.setAdapter(mMessageAdapter);
        mMainBtn = rootView.findViewById(R.id.btn_home_hall_quest_main);
        mDailyBtn = rootView.findViewById(R.id.btn_home_hall_quest_daily);
        mMessageBtn = rootView.findViewById(R.id.btn_home_hall_message);
        mMainBtn.setOnClickListener(this);
        mDailyBtn.setOnClickListener(this);
        mMessageBtn.setOnClickListener(this);

        setTitleName(rootView);
    }

    @Override
    public void onResume() {
        super.onResume();
        insertTestData();
        mListType = LIST_MAIN_QUEST;
        mMainRecyclerView.setVisibility(View.VISIBLE);
        mDailyRecyclerView.setVisibility(View.GONE);
        mMessageRecyclerView.setVisibility(View.GONE);
    }

    private void insertTestData(){
        mMainDataList = new LinkedList<>();
        mDailyDataList = new LinkedList<>();
        mMessageDataList = new LinkedList<>();
        for(int i=0; i<6; i++){
            Quest quest = new Quest();
            quest.setName("主線任務"+i);
            quest.setDesc(i+"我是描述我是描述我是描述我是描述");
            quest.setType(1);
            mMainDataList.add(quest);
        }
        mMainAdapter.setData(mMainDataList);
        for(int i=0; i<10; i++){
            Quest quest = new Quest();
            quest.setName("支線任務"+i);
            quest.setDesc(i+"我是描述我是描述我是描述我是描述");
            quest.setType(1);
            mDailyDataList.add(quest);
        }
        mDailyAdapter.setData(mDailyDataList);
        for(int i=0; i<12; i++){
            Message message = new Message();
            message.setWhen(new Date());
            message.setWhere(i+":testtesttesttesttesttest");
            mMessageDataList.add(message);
        }
        mMessageAdapter.setData(mMessageDataList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_home_hall_quest_main:
                mListType = LIST_MAIN_QUEST;
                break;
            case R.id.btn_home_hall_quest_daily:
                mListType = LIST_DAILY_QUEST;
                break;
            case R.id.btn_home_hall_message:
                mListType = LIST_MESSAGE;
                break;
        }

        mMainRecyclerView.setVisibility(mListType == LIST_MAIN_QUEST ? View.VISIBLE : View.GONE);
        mDailyRecyclerView.setVisibility( mListType == LIST_DAILY_QUEST ? View.VISIBLE : View.GONE);
        mMessageRecyclerView.setVisibility(mListType == LIST_MESSAGE? View.VISIBLE : View.GONE);
    }
}
