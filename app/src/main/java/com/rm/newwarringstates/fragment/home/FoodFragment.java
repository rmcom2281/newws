package com.rm.newwarringstates.fragment.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.home.FoodAdapter;
import com.rm.newwarringstates.fragment.HomeFragment;
import com.rm.newwarringstates.object.home.Food;
import com.rm.newwarringstates.utils.FollowerUtils;

import java.util.LinkedList;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;
import pl.droidsonroids.gif.GifImageView;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;

/**
 * Created by caia on 2018/1/17.
 */

public class FoodFragment extends HomeFragment implements View.OnClickListener {
    private final static String TAG = FoodFragment.class.getSimpleName();
    private Context mContext;
    private GifImageView mBackground;

    private List<Food> mWorkshopDataList;
    private List<Food> mTrainingDataList;

    private FoodAdapter mWorkshopAdapter;
    private FoodAdapter mTrainingAdapter;

    private RecyclerView mWorkshopRecyclerView;
    private RecyclerView mTrainingRecyclerView;

    private Button mWorkshopBtn;
    private Button mTrainingBtn;

    private int mListType = 0;
    private int LIST_WORKSHOP = 0;
    private int LIST_TRAINING = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_food, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        mWorkshopRecyclerView = rootView.findViewById(R.id.recycler_home_food_workshop);
        mWorkshopAdapter = new FoodAdapter(getActivity(), LIST_WORKSHOP, new FoodAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {
                itemOnClick(position);
            }
        });
        mWorkshopRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == SCROLL_STATE_IDLE){
                    mWorkshopAdapter.notifyDataSetChanged();
                }
            }
        });
        mWorkshopRecyclerView.setLayoutManager(linearLayoutManager);
        mWorkshopRecyclerView.setAdapter(mWorkshopAdapter);

        mTrainingRecyclerView = rootView.findViewById(R.id.recycler_home_food_training);
        mTrainingAdapter = new FoodAdapter(getActivity(), LIST_TRAINING, new FoodAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        final LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(LinearLayout.VERTICAL);
        mTrainingRecyclerView.setLayoutManager(linearLayoutManager2);
        mTrainingRecyclerView.setAdapter(mTrainingAdapter);

        mWorkshopBtn = rootView.findViewById(R.id.btn_home_food_workshop);
        mTrainingBtn = rootView.findViewById(R.id.btn_home_food_training);
        mWorkshopBtn.setOnClickListener(this);
        mTrainingBtn.setOnClickListener(this);

        setTitleName(rootView);
    }

    @Override
    public void onResume() {
        super.onResume();
        insertTestData();
        mListType = LIST_WORKSHOP;
        mWorkshopRecyclerView.setVisibility(View.VISIBLE);
        mTrainingRecyclerView.setVisibility(View.GONE);
    }

    private void insertTestData(){
        mWorkshopDataList = new LinkedList<>();
        mTrainingDataList = new LinkedList<>();
//        for(int i=0; i<6; i++){
//            Food data = new Food();
//            data.setTimeLeft(100000);
//            data.setComplete(false);
//            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
//            mWorkshopDataList.add(data);
//        }
        mWorkshopAdapter.setData(mWorkshopDataList);
//        for(int i=0; i<10; i++){
//            Food data = new Food();
//            data.setTimeLeft(100000);
//            data.setComplete(false);
//            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
//            mTrainingDataList.add(data);
//        }
        mTrainingAdapter.setData(mTrainingDataList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_home_food_workshop:
                mListType = LIST_WORKSHOP;
                break;
            case R.id.btn_home_food_training:
                mListType = LIST_TRAINING;
                break;
        }

        mWorkshopRecyclerView.setVisibility(mListType == LIST_WORKSHOP ? View.VISIBLE : View.GONE);
        mTrainingRecyclerView.setVisibility(mListType == LIST_TRAINING ? View.VISIBLE : View.GONE);
    }

    private void itemOnClick(int position){
        if(position==0){
            //add item
            Food data = new Food();
            data.setTimeLeft(30000);
            data.setComplete(false);
            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
            if(mListType == LIST_WORKSHOP){
                mWorkshopDataList.add(data);
                bindTimer(data);
                mWorkshopAdapter.add();
            } else{
                mTrainingDataList.add(data);
                mTrainingAdapter.add();
            }
        } else {
            if(mListType == LIST_WORKSHOP){
                Food data =  mWorkshopDataList.get(position-1);
                if(data.isComplete()){
                    data.setTimeLeft(30000);
                    data.setComplete(false);
                    mWorkshopAdapter.restart(position-1);
                } else {
                    Toast.makeText(mContext, data.getFollower().getName()+"'工作尚未完成", Toast.LENGTH_SHORT).show();
                }
                //mTrainingAdapter.notifyDataSetChanged();
            }

        }
    }

    List<CountdownView> times = new LinkedList<>();

    private void bindTimer(final Food food){
        CountdownView countdownView = new CountdownView(mContext);
        countdownView.start(food.getTimeLeft());
        countdownView.setOnCountdownIntervalListener(1000, new CountdownView.OnCountdownIntervalListener(){
            @Override
            public void onInterval(CountdownView cv, long remainTime) {
                food.setTimeLeft((int)remainTime);
            }
        });
        countdownView.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
            @Override
            public void onEnd(CountdownView cv) {
                food.setComplete(true);
            }
        });
        times.add(countdownView);
    }

    private void handleTimer(){

    }

}
