package com.rm.newwarringstates.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.home.WingAdapter;
import com.rm.newwarringstates.fragment.HomeFragment;
import com.rm.newwarringstates.object.home.Wing;
import com.rm.newwarringstates.utils.FollowerUtils;

import java.util.LinkedList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by caia on 2018/1/17.
 */

public class WingFragment extends HomeFragment implements View.OnClickListener {
    private GifImageView mBackground;

    private List<Wing> mTrainingDataList;

    private WingAdapter mTrainingAdapter;

    private RecyclerView mTrainingRecyclerView;

    private int mListType = 0;
    private int LIST_WORKSHOP = 0;
    private int LIST_TRAINING = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_wing, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){

        mTrainingRecyclerView = rootView.findViewById(R.id.recycler_home_wing_training);
        mTrainingAdapter = new WingAdapter(getActivity(), new WingAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        final LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(LinearLayout.VERTICAL);
        mTrainingRecyclerView.setLayoutManager(linearLayoutManager2);
        mTrainingRecyclerView.setAdapter(mTrainingAdapter);

        setTitleName(rootView);
    }

    @Override
    public void onResume() {
        super.onResume();
        insertTestData();
        mListType = LIST_WORKSHOP;
        mTrainingRecyclerView.setVisibility(View.VISIBLE);
    }

    private void insertTestData(){
        mTrainingDataList = new LinkedList<>();
        for(int i=0; i<10; i++){
            Wing data = new Wing();
            data.setTimeLeft(100000);
            data.setComplete(false);
            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
            mTrainingDataList.add(data);
        }
        mTrainingAdapter.setData(mTrainingDataList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
        }
    }
}
