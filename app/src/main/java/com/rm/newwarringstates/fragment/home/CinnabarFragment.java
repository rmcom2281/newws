package com.rm.newwarringstates.fragment.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.home.CinnabarAdapter;
import com.rm.newwarringstates.fragment.HomeFragment;
import com.rm.newwarringstates.object.home.Cinnabar;
import com.rm.newwarringstates.utils.FollowerUtils;

import java.util.LinkedList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by caia on 2018/1/17.
 */

public class CinnabarFragment extends HomeFragment implements View.OnClickListener {
    private GifImageView mBackground;

    private List<Cinnabar> mWorkshopDataList;
    private List<Cinnabar> mTrainingDataList;

    private CinnabarAdapter mWorkshopAdapter;
    private CinnabarAdapter mTrainingAdapter;

    private RecyclerView mWorkshopRecyclerView;
    private RecyclerView mTrainingRecyclerView;

    private Button mWorkshopBtn;
    private Button mTrainingBtn;

    private int mListType = 0;
    private int LIST_WORKSHOP = 0;
    private int LIST_TRAINING = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_cinnabar, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        mWorkshopRecyclerView = rootView.findViewById(R.id.recycler_home_cinnabar_workshop);
        mWorkshopAdapter = new CinnabarAdapter(getActivity(), LIST_WORKSHOP, new CinnabarAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        mWorkshopRecyclerView.setLayoutManager(linearLayoutManager);
        mWorkshopRecyclerView.setAdapter(mWorkshopAdapter);

        mTrainingRecyclerView = rootView.findViewById(R.id.recycler_home_cinnabar_training);
        mTrainingAdapter = new CinnabarAdapter(getActivity(), LIST_TRAINING, new CinnabarAdapter.ClickListener(){
            @Override
            public void onItemClick(int position, View v) {

            }
        });
        final LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        linearLayoutManager2.setOrientation(LinearLayout.VERTICAL);
        mTrainingRecyclerView.setLayoutManager(linearLayoutManager2);
        mTrainingRecyclerView.setAdapter(mTrainingAdapter);

        mWorkshopBtn = rootView.findViewById(R.id.btn_home_cinnabar_workshop);
        mTrainingBtn = rootView.findViewById(R.id.btn_home_cinnabar_training);
        mWorkshopBtn.setOnClickListener(this);
        mTrainingBtn.setOnClickListener(this);

        setTitleName(rootView);
    }

    @Override
    public void onResume() {
        super.onResume();
        insertTestData();
        mListType = LIST_WORKSHOP;
        mWorkshopRecyclerView.setVisibility(View.VISIBLE);
        mTrainingRecyclerView.setVisibility(View.GONE);
    }

    private void insertTestData(){
        mWorkshopDataList = new LinkedList<>();
        mTrainingDataList = new LinkedList<>();
        for(int i=0; i<6; i++){
            Cinnabar data = new Cinnabar();
            data.setTimeLeft(100000);
            data.setComplete(false);
            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
            mWorkshopDataList.add(data);
        }
        mWorkshopAdapter.setData(mWorkshopDataList);
        for(int i=0; i<10; i++){
            Cinnabar data = new Cinnabar();
            data.setTimeLeft(100000);
            data.setComplete(false);
            data.setFollower(FollowerUtils.generateNewFollower(1).get(0));
            mTrainingDataList.add(data);
        }
        mTrainingAdapter.setData(mTrainingDataList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_home_cinnabar_workshop:
                mListType = LIST_WORKSHOP;
                break;
            case R.id.btn_home_cinnabar_training:
                mListType = LIST_TRAINING;
                break;
        }

        mWorkshopRecyclerView.setVisibility(mListType == LIST_WORKSHOP ? View.VISIBLE : View.GONE);
        mTrainingRecyclerView.setVisibility(mListType == LIST_TRAINING ? View.VISIBLE : View.GONE);
    }
}
