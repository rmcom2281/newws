package com.rm.newwarringstates.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.data.BuildingEnum;
import com.rm.newwarringstates.data.Constants;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created by User on 2017/12/30.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    private ImageView mCover;
    private int mBuildingType;
    private GifImageView mBackground;

    private String mBuildingName;

    public static HomeFragment newInstance(int type) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARGS_TYPE_HOME_BUILDING, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mBuildingType = getArguments().getInt(Constants.ARGS_TYPE_HOME_BUILDING, 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        BuildingEnum buildingEnum = BuildingEnum.HALL;
        Object[] possibleValues = buildingEnum.getDeclaringClass().getEnumConstants();
        mBuildingName = buildingEnum.getName();
        for (Object o : possibleValues) {
            BuildingEnum object = (BuildingEnum) o;
            if (object.getId() == mBuildingType) {
                buildingEnum = object;
                mBuildingName = buildingEnum.getName();
                break;
            }
        }
        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setTitleName(View view) {
        TextView name = view.findViewById(R.id.txt_home_title);
        name.setText(mBuildingName);
    }

}