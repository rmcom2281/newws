package com.rm.newwarringstates.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rm.newwarringstates.R;

/**
 * Created by User on 2017/12/30.
 */

public class CountryFragment extends Fragment {

    private String mCountryName = "";

    public static CountryFragment newInstance(String name){
        CountryFragment fragment = new CountryFragment();
        Bundle args = new Bundle();
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCountryName =  getArguments().getString("name", "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_country, container, false);
        TextView countryNameTextView = rootView.findViewById(R.id.country_name);
        countryNameTextView.setText(mCountryName);
        return rootView;
    }

}