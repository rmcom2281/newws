package com.rm.newwarringstates.adapter.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.home.Message;

import java.util.List;

/**
 * Created by caia on 2018/1/2.
 */

public class HallMessageAdapter extends RecyclerView.Adapter<HallMessageAdapter.ViewHolder> {

    private Context mContext;
    private List<Message> mDataList;
    private ClickListener mClickListener;

    public HallMessageAdapter(Context context, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
    }

    public void  setData(List<Message> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_hall_message, parent, false);
        ItemViewHolder ivh = new ItemViewHolder(view);
        return ivh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemViewHolder ivh = (ItemViewHolder) holder;
        Message data = mDataList.get(position);
        ivh.when.setText(data.getWhen().toString());
        ivh.desc.setText(data.getWhere());
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class ItemViewHolder extends ViewHolder{

        //private ImageView eye;
        private TextView when;
        private TextView desc;

        public ItemViewHolder(View v) {
            super(v);
            when = v.findViewById(R.id.list_txt_home_hall_message_when);
            desc = v.findViewById(R.id.list_txt_home_hall_message_desc);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
