package com.rm.newwarringstates.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.Follower;
import com.rm.newwarringstates.utils.FollowerUtils;

import java.util.List;

/**
 * Created by caia on 2018/1/2.
 */

public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.ViewHolder> {

    private static int HEADER_VIEW = 99;
    private Context mContext;
    private List<Follower> mDataList;
    private ClickListener mClickListener;

    public FollowerAdapter(Context context, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
    }

    public void  setData(List<Follower> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == HEADER_VIEW){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_follower_h, parent, false);
            HeaderViewHolder hvh = new HeaderViewHolder(view);
            return hvh;
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_follower, parent, false);
        FollowerViewHolder pvh = new FollowerViewHolder(view);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(holder instanceof FollowerViewHolder){
            FollowerViewHolder pvh = (FollowerViewHolder) holder;
            Follower data = mDataList.get(position-1);
            pvh.name.setText(data.getName());
            List<Integer> reslist = FollowerUtils.getFollowRes(data);
            pvh.body.setBackgroundResource(reslist.get(3));
            pvh.face.setBackgroundResource(reslist.get(0));
            pvh.head.setBackgroundResource(reslist.get(1));
            pvh.mouth.setBackgroundResource(reslist.get(2));
        }
    }

    @Override
    public int getItemCount() {
        if(mDataList == null){
            return 0;
        }
        if(mDataList.size() == 0){
            return 1;
        }
        return mDataList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return HEADER_VIEW;
        }
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class HeaderViewHolder extends ViewHolder{

        public HeaderViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }

    public class FollowerViewHolder  extends ViewHolder{

        //private ImageView eye;
        private TextView name;
        private Button detail;
        private ImageView body;
        private ImageView face;
        private ImageView head;
        private ImageView mouth;

        public FollowerViewHolder(View v) {
            super(v);
            body = v.findViewById(R.id.follower_list_lay_cover_body);
            face = v.findViewById(R.id.follower_list_lay_cover_face);
            head = v.findViewById(R.id.follower_list_lay_cover_head);
            mouth = v.findViewById(R.id.follower_list_lay_cover_mouth);
            name = v.findViewById(R.id.follower_list_name);
            detail = v.findViewById(R.id.follower_list_detail);
            detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
