package com.rm.newwarringstates.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;
import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.widge.ExpandableItemIndicator;
import com.rm.newwarringstates.object.City;
import com.rm.newwarringstates.object.Country;

import java.util.List;

/**
 * Created by caia on 2018/1/4.
 */

public class ExpandableCountryAdapter extends AbstractExpandableItemAdapter<ExpandableCountryAdapter.CountryViewHolder, ExpandableCountryAdapter.CityViewHolder> {

    private List<Country> mDataList;

    public ExpandableCountryAdapter(List<Country> dataList){
        mDataList = dataList;
        setHasStableIds(true);
    }

    @Override
    public int getGroupCount() {
        return mDataList.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        return mDataList.get(groupPosition).getCities().size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mDataList.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mDataList.get(groupPosition).getCities().get(childPosition).getId();
    }

    @Override
    public ExpandableCountryAdapter.CountryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater =LayoutInflater.from(parent.getContext());
        final View v = layoutInflater.inflate(R.layout.list_item_country, parent, false);
        return new CountryViewHolder(v);
    }

    @Override
    public ExpandableCountryAdapter.CityViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater =LayoutInflater.from(parent.getContext());
        final View v = layoutInflater.inflate(R.layout.list_item_city, parent, false);
        return  new CityViewHolder(v);
    }

    @Override
    public void onBindGroupViewHolder(CountryViewHolder holder, int groupPosition, int viewType) {
        final Country item = mDataList.get(groupPosition);

        // set text
        holder.countryName.setText(item.getName());

        // mark as clickable
        holder.itemView.setClickable(true);

        // set background resource (target view ID: container)
        final int expandState = holder.getExpandStateFlags();

        if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_UPDATED) != 0) {
            int bgResId;
            boolean isExpanded;
            boolean animateIndicator = ((expandState & ExpandableItemConstants.STATE_FLAG_HAS_EXPANDED_STATE_CHANGED) != 0);

            if ((expandState & ExpandableItemConstants.STATE_FLAG_IS_EXPANDED) != 0) {
                bgResId = R.drawable.bg_country_expanded;
                isExpanded = true;
            } else {
                bgResId = R.drawable.bg_country_normal;
                isExpanded = false;
            }

            holder.countryContainer.setBackgroundResource(bgResId);
            holder.itemIndicator.setExpandedState(isExpanded, animateIndicator);
        }
    }

    @Override
    public void onBindChildViewHolder(CityViewHolder holder, int groupPosition, int childPosition, int viewType) {
        final City item = mDataList.get(groupPosition).getCities().get(childPosition);

        // set text
        holder.cityName.setText(item.getName());

        // set background resource (target view ID: container)
        int bgResId;
        bgResId = R.drawable.bg_city_normal;
        holder.cityContainer.setBackgroundResource(bgResId);
    }

    @Override
    public boolean onCheckCanExpandOrCollapseGroup(CountryViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        // check the item is *not* pinned
//        if (mProvider.getGroupItem(groupPosition).isPinned()) {
//            // return false to raise View.OnClickListener#onClick() event
//            return false;
//        }

        // check is enabled
        if (!(holder.itemView.isEnabled() && holder.itemView.isClickable())) {
            return false;
        }

        return true;
    }


    public static abstract class BaseViewHolder extends AbstractExpandableItemViewHolder{

        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class CountryViewHolder extends BaseViewHolder{
        public ExpandableItemIndicator itemIndicator;
        public TextView countryName;
        public FrameLayout countryContainer;
        public CountryViewHolder(View itemView) {
            super(itemView);
            itemIndicator = itemView.findViewById(R.id.list_country_indicator);
            countryName = itemView.findViewById(R.id.list_country_name);
            countryContainer = itemView.findViewById(R.id.list_country_container);
        }
    }

    public static class CityViewHolder extends BaseViewHolder{
        public TextView cityName;
        public FrameLayout cityContainer;
        public CityViewHolder(View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.list_city_name);
            cityContainer = itemView.findViewById(R.id.list_city_container);
        }
    }
}
