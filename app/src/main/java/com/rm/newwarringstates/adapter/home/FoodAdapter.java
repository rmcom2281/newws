package com.rm.newwarringstates.adapter.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.Follower;
import com.rm.newwarringstates.object.home.Food;

import java.util.List;

import cn.iwgang.countdownview.CountdownView;

/**
 * Created by caia on 2018/1/2.
 */

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder> {

    private static int HEADER_VIEW = 99;
    public static int WORKSHOP  = 0;
    public static int TRAINING   = 1;

    private Context mContext;
    private List<Food> mDataList;
    private ClickListener mClickListener;
    private int mType = 0;

    public FoodAdapter(Context context, int type, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
        mType = type;
    }

    public void setData(List<Food> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    public void add(){
        notifyItemInserted(mDataList.size());
    }

    public void restart(int position){
        mDataList.get(position).setTimeLeft(30000);
        mDataList.get(position).setComplete(false);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == HEADER_VIEW){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_food_h, parent, false);
            HeaderViewHolder hvh = new HeaderViewHolder(view);
            return hvh;
        }
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_food, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(holder instanceof ItemViewHolder){

            final ItemViewHolder vh = (ItemViewHolder) holder;
            final Food data = mDataList.get(position-1);
            final Follower follower = data.getFollower();
//            if(data.getTimeLeft()>0){
//                data.setComplete(false);
//            } else{
//                data.setComplete(true);
//            }
            Log.e("TEST",  "ID:"+position+"="+follower.getName()+"; !isComplete= "+!data.isComplete()+";leftTime="+data.getTimeLeft());
            if(!data.isComplete()){
                if(mType == WORKSHOP){
                    vh.title.setText("烹飪中...");
                    vh.desc.setText(follower.getName()+" 正在努力烹飪中");
                } else {
                    vh.title.setText("訓練中...");
                    vh.desc.setText(follower.getName()+" 正在努力訓練廚藝中");
                }
                vh.countdown.setVisibility(View.VISIBLE);
                vh.countdown.start(data.getTimeLeft());
                vh.countdown.setOnCountdownEndListener(new CountdownView.OnCountdownEndListener() {
                    @Override
                    public void onEnd(CountdownView cv) {
                        if(mType == WORKSHOP){
                            vh.title.setText("已完成烹飪!");
                            vh.desc.setText("點擊收取: "+follower.getName()+"已生產1999份糧食!");
                        } else {
                            vh.title.setText("已完成訓練!");
                            vh.desc.setText("點擊完成: "+follower.getName()+"已提高自身廚藝技巧!");
                        }
                        vh.countdown.setVisibility(View.GONE);
                    }
                });
            } else {
                if(mType == WORKSHOP){
                    vh.title.setText("已完成烹飪!");
                    vh.desc.setText("點擊收取: "+follower.getName()+"已生產1999份糧食!");
                } else {
                    vh.title.setText("已完成訓練!");
                    vh.desc.setText("點擊完成: "+follower.getName()+"已提高自身廚藝技巧!");
                }
                vh.countdown.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if(mDataList == null){
            return 0;
        }
        if(mDataList.size() == 0){
            return 1;
        }
        return mDataList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return HEADER_VIEW;
        }
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class HeaderViewHolder extends ViewHolder{

        public HeaderViewHolder(View v) {
            super(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }

    public class ItemViewHolder  extends ViewHolder{

        //private ImageView eye;
        private TextView title;
        private TextView desc;
        private ImageView cover;
        private CountdownView countdown;

        public ItemViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.list_txt_food_title);
            desc = v.findViewById(R.id.list_txt_food_desc);
            cover = v.findViewById(R.id.list_img_food_cover);
            countdown = v.findViewById(R.id.list_countdown_food);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
