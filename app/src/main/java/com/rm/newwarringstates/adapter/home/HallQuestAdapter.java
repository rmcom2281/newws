package com.rm.newwarringstates.adapter.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.home.Quest;

import java.util.List;

/**
 * Created by caia on 2018/1/2.
 */

public class HallQuestAdapter extends RecyclerView.Adapter<HallQuestAdapter.ViewHolder> {

    private Context mContext;
    private List<Quest> mDataList;
    private ClickListener mClickListener;

    public HallQuestAdapter(Context context, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
    }

    public void  setData(List<Quest> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_hall_quest, parent, false);
        QuestViewHolder qvh = new QuestViewHolder(view);
        return qvh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QuestViewHolder qvh = (QuestViewHolder) holder;
        Quest data = mDataList.get(position);
        qvh.title.setText(data.getName());
        qvh.desc.setText(data.getDesc());
        if(data.getType()==1){

        } else {

        }
        qvh.progress.setText("(0/1)");
        if(data.isCheck()){

        } else {

        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class QuestViewHolder extends ViewHolder{

        //private ImageView eye;
        private TextView title;
        private TextView desc;
        private TextView progress;
        private ImageView type;

        public QuestViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.list_txt_home_hall_quest_title);
            desc = v.findViewById(R.id.list_txt_home_hall_quest_desc);
            progress = v.findViewById(R.id.list_txt_home_hall_quest_progress);
            type = v.findViewById(R.id.list_img_home_hall_quest_title);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
