package com.rm.newwarringstates.adapter.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.home.Book;
import com.rm.newwarringstates.object.home.Wing;

import java.util.List;

import cn.iwgang.countdownview.CountdownView;

/**
 * Created by caia on 2018/1/2.
 */

public class WingAdapter extends RecyclerView.Adapter<WingAdapter.ViewHolder> {

    private Context mContext;
    private List<Wing> mDataList;
    private ClickListener mClickListener;

    public WingAdapter(Context context, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
    }

    public void  setData(List<Wing> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wing, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemViewHolder vh = (ItemViewHolder) holder;
        Wing data = mDataList.get(position);

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class ItemViewHolder extends ViewHolder{

        private TextView title;
        private TextView desc;
        private ImageView cover;
        private CountdownView countdown;

        public ItemViewHolder(View v) {
            super(v);

            title = v.findViewById(R.id.list_txt_wing_title);
            desc = v.findViewById(R.id.list_txt_wing_desc);
            cover = v.findViewById(R.id.list_img_wing_cover);
            countdown = v.findViewById(R.id.list_countdown_wing);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
