package com.rm.newwarringstates.adapter.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.home.Book;

import java.util.List;

import cn.iwgang.countdownview.CountdownView;

/**
 * Created by caia on 2018/1/2.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    private Context mContext;
    private List<Book> mDataList;
    private ClickListener mClickListener;

    public BookAdapter(Context context, ClickListener clickListener){
        mContext = context;
        mClickListener = clickListener;
    }

    public void  setData(List<Book> dataList){
        mDataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_book, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemViewHolder vh = (ItemViewHolder) holder;
        Book data = mDataList.get(position);

    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public  interface ClickListener{
        void onItemClick(int position, View v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(View v){
            super(v);
        }
    }

    public class ItemViewHolder extends ViewHolder{

        private TextView title;
        private TextView desc;
        private ImageView cover;
        private CountdownView countdown;

        public ItemViewHolder(View v) {
            super(v);

            title = v.findViewById(R.id.list_txt_book_title);
            desc = v.findViewById(R.id.list_txt_book_desc);
            cover = v.findViewById(R.id.list_img_book_cover);
            countdown = v.findViewById(R.id.list_countdown_book);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mClickListener.onItemClick(getAdapterPosition(), view);
                }
            });
        }
    }
}
