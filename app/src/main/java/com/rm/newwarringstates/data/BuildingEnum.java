package com.rm.newwarringstates.data;

/**
 * Created by User on 2018/1/6.
 */

public enum  BuildingEnum {
    HALL(0, "大廳", 0),
    WING(1, "廂房", 0),
    BOOK(2, "書房", 0),
    FOOD(3, "食堂", 0),
    HORSE(4, "馬廄", 0),
    ALCHEMY(5, "煉丹房", 0),
    BLACKSMITH(6, "鐵舖", 0);


    private final int id;
    private String name;
    private int resId;

    BuildingEnum(final int id, String name, int resId){
        this.id = id;
        this.name = name;
        this.resId = resId;
    }

    public final int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getResId() {
        return resId;
    }

}
