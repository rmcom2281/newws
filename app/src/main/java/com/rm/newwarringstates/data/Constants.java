package com.rm.newwarringstates.data;

/**
 * Created by caia on 2018/1/2.
 */

public class Constants {

    public static String ARGS_NAME_PLAYER_ID = "PLAYER_ID";
    public static String ARGS_TYPE_HOME_BUILDING = "HOME_BUILDING_TYPE";
    public static String ARGS_TYPE_HOME_BUILDING_NAME = "HOME_BUILDING_NAME";

}
