package com.rm.newwarringstates.utils;

import android.util.Log;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.Follower;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by caia on 2017/12/28.
 */

public class FollowerUtils {
    private static List<String> secondNames = new ArrayList<String>(Arrays.asList("趙", "錢", "孫", "李", "周", "吳", "鄭", "王"));
    private static List<String> firstNames = new ArrayList<String>(Arrays.asList("小明", "小華", "家豪", "宗翰", "建宏", "俊傑", "冠宇", "宗憲"));

    public static List<Follower> generateNewFollower(int count){
        List<Follower> result = new ArrayList<>();
        for(int i=0; i<count; i++){
            Follower follower = new Follower();
            follower.setAgility(RandomUtils.generateRandomValue(1, 100));
            follower.setForce(RandomUtils.generateRandomValue(1, 100));
            follower.setIntelligence(RandomUtils.generateRandomValue(1, 100));
            follower.setCharm(RandomUtils.generateRandomValue(1, 100));
            int id = i+1;
            follower.setId(id);
            String secondName = secondNames.get(RandomUtils.generateRandomValue(1, 8));
            String firstName = firstNames.get(RandomUtils.generateRandomValue(1, 8));
            follower.setName(secondName+firstName);
            follower.setFaceType(RandomUtils.generateRandomValue(1, 4));
            follower.setBodyType(RandomUtils.generateRandomValue(1, 7));
            follower.setMouthType(RandomUtils.generateRandomValue(1, 6));
            result.add(follower);
        }
        return result;
    }

    //hardcore now,  hope find some smart funtion
    public static List<Integer> getFollowRes(Follower follower){
        List<Integer> resList = new ArrayList<>();
        int face = R.drawable.face1;
        int header = R.drawable.header1;
        int mouth = R.drawable.down1;
        int body = R.drawable.body1;
        switch(follower.getFaceType()){
            case 0:
                face = R.drawable.face1;
                break;
            case 1:
                face = R.drawable.face2;
                break;
            case 2:
                face = R.drawable.face3;
                break;
            case 3:
                face = R.drawable.face4;
                break;
        }
        switch(follower.getBodyType()){
            case 0:
                header = R.drawable.header1;
                body = R.drawable.body1;
                break;
            case 1:
                header = R.drawable.header2;
                body = R.drawable.body2;
                break;
            case 2:
                header = R.drawable.header3;
                body = R.drawable.body3;
                break;
            case 3:
                header = R.drawable.header4;
                body = R.drawable.body4;
                break;
            case 4:
                header = R.drawable.header5;
                body = R.drawable.body5;
                break;
            case 5:
                header = R.drawable.header6;
                body = R.drawable.body6;
                break;
            case 6:
                header = R.drawable.header7;
                body = R.drawable.body7;
                break;
        }

        switch(follower.getMouthType()){
            case 0:
                mouth = R.drawable.down1;
                break;
            case 1:
                mouth = R.drawable.down2;
                break;
            case 2:
                mouth = R.drawable.down3;
                break;
            case 3:
                mouth = R.drawable.down4;
                break;
            case 4:
                mouth = R.drawable.down5;
                break;
            case 5:
                mouth = R.drawable.down6;
                break;
        }
        resList.add(face);
        resList.add(header);
        resList.add(mouth);
        resList.add(body);
        return resList;
    }

}
