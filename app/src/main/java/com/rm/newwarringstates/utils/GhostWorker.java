package com.rm.newwarringstates.utils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by caia on 2018/1/9.
 */

public class GhostWorker {

    private List<Job> jobs = new LinkedList<>();

    public void initData(List<Job> input){
        clearData();
        jobs.addAll(input);
    }

    public void clearData(){
        jobs = new LinkedList<>();
    }

    public boolean setData(int position, Job job){
        if(jobs.size()>position){
            jobs.set(position, job);
            return true;
        }
        return false;
    }

    class Job {
        private int id;
        private int seq;
        private int time;
        private int type;
        private String desc;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

}
