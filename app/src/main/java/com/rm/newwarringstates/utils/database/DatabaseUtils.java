package com.rm.newwarringstates.utils.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

/**
 * Created by User on 2018/1/11.
 */

public class DatabaseUtils {

    public static void init(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference serverTime = database.getReference("server_time");
        serverTime.setValue(ServerValue.TIMESTAMP);
    }
}
