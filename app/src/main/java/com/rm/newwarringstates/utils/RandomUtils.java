package com.rm.newwarringstates.utils;


import android.util.Log;

public class RandomUtils {

    public static int generateRandomValue(int min, int max){
        Double random = Math.random();
        int range = Math.max(1, max-min+1);
        Double rangeD = ((double) range)*random;

        //Log.e("test", "random="+random+"; range="+range+"; return="+rangeD);
        return rangeD.intValue();
    }
}
