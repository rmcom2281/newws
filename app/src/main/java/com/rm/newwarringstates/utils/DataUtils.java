package com.rm.newwarringstates.utils;

import android.content.Context;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.object.City;
import com.rm.newwarringstates.object.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by caia on 2018/1/3.
 */

public class DataUtils {

    private static String NODE_COUNTRIES = "countries";
    private static String NODE_ID = "id";
    private static String NODE_NAME = "name";
    private static String NODE_CITIES = "cities";

    public static List<Country> getCountryInfo(Context context) {
        List<Country> countries = new ArrayList<>();
        JSONObject objects = getObjectFromRaw(context, R.raw.country_info);
        try {
            JSONArray countryArray = objects.getJSONArray(NODE_COUNTRIES);
            for(int i = 0; i<countryArray.length(); i++){
                Country country = new Country();
                country.setId(countryArray.getJSONObject(i).getInt(NODE_ID));
                country.setName(countryArray.getJSONObject(i).getString(NODE_NAME));
                List<City> cities = new ArrayList<>();
                JSONArray cityArray = countryArray.getJSONObject(i).getJSONArray(NODE_CITIES);
                for(int j = 0; j<cityArray.length(); j++){
                    City city = new City();
                    city.setId(cityArray.getJSONObject(j).getInt(NODE_ID));
                    city.setName(cityArray.getJSONObject(j).getString(NODE_NAME));
                    cities.add(city);
                }
                country.setCities(cities);
                countries.add(country);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return countries;
    }

    public static JSONObject getObjectFromRaw(Context context, int resId){
        JSONObject object = new JSONObject();
        try {
            InputStream is = context.getResources().openRawResource(resId);
            ByteArrayOutputStream os = new ByteArrayOutputStream();

            int ctr = is.read();
            while (ctr != -1){
                os.write(ctr);
                ctr = is.read();
            }
            is.close();
            object = new JSONObject(os.toString());
        } catch (Exception e){
            e.printStackTrace();
        }
        return object;
    }
}
