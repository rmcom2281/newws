package com.rm.newwarringstates.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rm.newwarringstates.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button mStartBtn;
    private TextView mContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mStartBtn = findViewById(R.id.main_start);
        mContent = findViewById(R.id.main_text);
        mStartBtn.setOnClickListener(this);
//        getSupportActionBar().hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.main_start:
//               List<Follower> persons = FollowerUtils.generateNewPerson(10);
//                StringBuffer stringBuffer = new StringBuffer();
//                for(Follower person :  persons){
//                    stringBuffer.append(person.getName()+"("+person.getForce()+","+person.getIntelligence()+","+person.getAgility()+","+person.getCharm()+")"+"\n");
//                }
//                mContent.setText(stringBuffer.toString());

//                Intent intent = new Intent();
//                intent.setClass(this, CountryActivity.class);
//                startActivity(intent);
//                finish();

//                new FollowerFragment().newInstance(1).show(getSupportFragmentManager(), "test");

//                Intent intent = new Intent();
//                intent.setClass(this, BaseFragmentActivity.class);
//                startActivity(intent);
//                finish();

                Intent intent = new Intent();
                intent.setClass(this, HomeActivity.class);
                startActivity(intent);
                finish();
                break;

        }
    }
}
