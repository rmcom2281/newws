package com.rm.newwarringstates.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.CountryPagerAdapter;
import com.rm.newwarringstates.fragment.CountryFragment;
import com.rm.newwarringstates.object.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryActivity extends AppCompatActivity{
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private List<Country> countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        getSupportActionBar().hide();
        mViewPager =  findViewById(R.id.country_viewpager);
        mTabLayout =  findViewById(R.id.country_tab);
        initData();
        initView();
    }


    private void setupViewPager(ViewPager viewPager) {
        CountryPagerAdapter adapter = new CountryPagerAdapter(getSupportFragmentManager());
        for(Country country : countryList){
            adapter.addFragment(CountryFragment.newInstance(country.getName()), country.getName());
        }
        viewPager.setAdapter(adapter);
    }

    private void initData() {
        countryList = new ArrayList<>();
        Country c1 = new Country();
        c1.setId(0);
        c1.setName("魏");
        countryList.add(c1);
        Country c2 = new Country();
        c2.setId(1);
        c2.setName("齊");
        countryList.add(c2);
        Country c3 = new Country();
        c3.setId(2);
        c3.setName("楚");
        countryList.add(c3);
    }

    private void initView() {
        for(Country country : countryList){
            mTabLayout.addTab(mTabLayout.newTab().setText(country.getName()));
        }
        setupViewPager(mViewPager);
        initListener();
    }

    private void initListener() {
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

}
