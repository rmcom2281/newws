package com.rm.newwarringstates.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.WindowManager;

import com.rm.newwarringstates.R;
import com.rm.newwarringstates.fragment.CountryInfoFragment;
import com.rm.newwarringstates.fragment.FollowerFragment;

public class BaseFragmentActivity extends FragmentActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_fragment);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        FragmentManager fragMgr = getSupportFragmentManager();
        FollowerFragment followerFragment = new FollowerFragment().newInstance(1);
        fragMgr.beginTransaction()
                .add(R.id.base_fragment, followerFragment)
                .commit();
    }
}
