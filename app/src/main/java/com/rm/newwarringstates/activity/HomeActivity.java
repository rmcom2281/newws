package com.rm.newwarringstates.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.rm.newwarringstates.R;
import com.rm.newwarringstates.adapter.HomePagerAdapter;
import com.rm.newwarringstates.data.BuildingEnum;
import com.rm.newwarringstates.fragment.HomeFragment;
import com.rm.newwarringstates.fragment.home.BookFragment;
import com.rm.newwarringstates.fragment.home.CinnabarFragment;
import com.rm.newwarringstates.fragment.home.FoodFragment;
import com.rm.newwarringstates.fragment.home.GoldFragment;
import com.rm.newwarringstates.fragment.home.HallFragment;
import com.rm.newwarringstates.fragment.home.HorseFragment;
import com.rm.newwarringstates.fragment.home.WingFragment;
import com.rm.newwarringstates.object.Player;
import com.rm.newwarringstates.object.database.TestTask;
import com.rm.newwarringstates.view.WSProgressBar;

import java.util.Date;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{
    private final String TAG = HomeActivity.class.getSimpleName();

    private Context mContext;
    private TabLayout mTabs;
    private ViewPager mViewPager;
    private HomePagerAdapter mHomeAdapter;
    private FloatingActionButton mFollowersBtn;
    private WSProgressBar mFood, mFollower, mGold, mHorse, mCinnabar;

    private Date serverDate = new Date();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference myRef = database.getReference("message");
    final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //getSupportActionBar().hide();
        mContext = this;
        mTabs = findViewById(R.id.home_tab);
        mViewPager = findViewById(R.id.home_viewpager);
        mFollowersBtn = findViewById(R.id.home_btn_follower);
        mFollowersBtn.setOnClickListener(this);
        initView();
        firstTime = true;
    }

    private void initView(){
        BuildingEnum buildingEnum = BuildingEnum.HALL;
        Object[] objects = buildingEnum.getDeclaringClass().getEnumConstants();
        mHomeAdapter = new HomePagerAdapter(getSupportFragmentManager());
        for(Object object : objects){
            BuildingEnum objecEnum = (BuildingEnum) object;
            mTabs.addTab(mTabs.newTab().setText(objecEnum.getName()));
            HomeFragment fragment;

            switch (objecEnum){
                case HALL:
                    fragment = new HallFragment();
                    break;

                case FOOD:
                    fragment = new FoodFragment();
                    break;

                case HORSE:
                    fragment = new HorseFragment();
                    break;

                case BLACKSMITH:
                    fragment = new GoldFragment();
                    break;

                case ALCHEMY:
                    fragment = new CinnabarFragment();
                    break;

                case BOOK:
                    fragment = new BookFragment();
                    break;

                case WING:
                    fragment = new WingFragment();
                    break;

                default:
                    fragment= HomeFragment.newInstance(objecEnum.getId());
                    break;
            }

            mHomeAdapter.addFragment(fragment, objecEnum.getName());
        }

        //mHomeAdapter.addFragment(new HallFragment(), "HALL");
        mViewPager.setAdapter(mHomeAdapter);

        //mFollower = findViewById(R.id.home_progress_horse);
        mFood = findViewById(R.id.home_progress_food);
        mGold = findViewById(R.id.home_progress_gold);
        mHorse = findViewById(R.id.home_progress_horse);
        mCinnabar = findViewById(R.id.home_progress_cinnabar);
        initListener();
    }

    private void initListener() {
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
    }

    private int testint = 1;
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.home_btn_follower:
//                Intent intent = new Intent();
//                intent.setClass(this, BaseFragmentActivity.class);
//                startActivity(intent);

                //new FollowerFragment().newInstance(1).show(getSupportFragmentManager(), "ShowFollowers");
                gettime();
//                testTimer1.start(10000);
                addOneRecord(testint);
                testint = testint+1;
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
        gettime();

    }

    private void initData(){
        Player player = new Player();
        mFood.setText(this, 59487, 99999);
        mGold.setText(this, 12345, 99999);
        mHorse.setText(this, 1000, 99999);
        mCinnabar.setText(this, 95666, 99999);

        //mFood.setText(String.valueOf(player.getFood())+" ");
        //mFollower.setText(player.getFollower()+"/10 ");
        //mGold.setText(String.valueOf(player.getGold())+" ");
//        mHorse.setText(String.valueOf(player.getHorse())+" ");
//        mCinnabar.setText(String.valueOf(player.getCinnabar())+" ");
    }

    private void addOneRecord(int i){
        TestTask testTask = new TestTask(i, 10000*i, ServerValue.TIMESTAMP);
        mDatabase.child("test").child(String.valueOf(testint)).setValue(testTask);
    }

    private void gettime(){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                long value = dataSnapshot.getValue(Long.class);

                serverDate.setTime(value);
                Log.e(TAG, "Value is: " + value+"'; DATE="+serverDate);
                //myRef.setValue(ServerValue.TIMESTAMP);
                if(firstTime){
                    firstTime = false;
                } else {
                    onGetData();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
    private boolean firstTime = true;
    private void onGetData(){
        DatabaseReference ref6 = mDatabase.child("test").child("6");
        ref6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TestTask testTask = dataSnapshot.getValue(TestTask.class);
                long time = (long) testTask.now;
                long timetostop = time+testTask.needTime;
                Date date = new Date();
                date.setTime(timetostop);
                int offset = (int)(date.getTime() - serverDate.getTime());
                int needtime = offset > 0 ? offset : 0;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference ref7 = mDatabase.child("test").child("7");
        ref7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TestTask testTask = dataSnapshot.getValue(TestTask.class);
                long time = (long) testTask.now;
                long timetostop = time+testTask.needTime;
                Date date = new Date();
                date.setTime(timetostop);
                int offset = (int)(date.getTime() - serverDate.getTime());
                int needtime = offset > 0 ? offset : 0;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference ref8 = mDatabase.child("test").child("8");
        ref8.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                TestTask testTask = dataSnapshot.getValue(TestTask.class);
                long time = (long) testTask.now;
                long timetostop = time+testTask.needTime;
                Date date = new Date();
                date.setTime(timetostop);
                int offset = (int)(date.getTime() - serverDate.getTime());
                int needtime = offset > 0 ? offset : 0;
                Log.e("test", "server ="+serverDate+";date= "+date+";offset="+offset+";need="+needtime);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
