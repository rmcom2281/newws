package com.rm.newwarringstates.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by User on 2018/1/7.
 */

public class TimerView extends View {
    private Paint hrPaint, minPaint, secPaint;
    private String hrStr, minStr,secStr;
    private int time;
    private Handler mHandler;
    private Runnable run = new Runnable() {
        @Override
        public void run() {
            time = (time -1) >=0 ? (time -1) : 0;
            parseTime(time);
            drawableStateChanged();
            if(time>0){
                mHandler.postDelayed(this, 1000);
            }
        }
    };

    public TimerView(Context context) {
        super(context);
        initPaint(context);
    }

    public TimerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint(context);
    }

    public TimerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint(context);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect bounds = new Rect();
        hrPaint.getTextBounds(hrStr, 0, hrStr.length(), bounds);
        int x = getWidth() / 2 - bounds.centerX()-100;
        int y = getHeight() / 2 - bounds.centerY();
        canvas.drawText(hrStr, x, y, hrPaint);
        Rect bounds2 = new Rect();
        minPaint.getTextBounds(minStr, 0, minStr.length(), bounds2);
        int x1 = getWidth() / 2 - bounds.centerX()-50;
        int y1 = getHeight() / 2 - bounds.centerY();
        canvas.drawText(minStr, x1, y1, minPaint);
        Rect bounds3 = new Rect();
        secPaint.getTextBounds(secStr, 0, secStr.length(), bounds3);
        int x2 = getWidth() / 2 - bounds.centerX();
        int y2 = getHeight() / 2 - bounds.centerY();
        canvas.drawText(secStr, x2, y2, secPaint);
    }

    public synchronized void setTime(int time) {
        this.time = time;
        mHandler.postDelayed(run, 1000);
    }

    private void initPaint(Context context){
        time = 0;
        hrStr = "0:";
        minStr = "00:";
        secStr = "00";
        hrPaint = new Paint();
        hrPaint.setColor(Color.WHITE);
        hrPaint.setTextSize(40);
        //hrPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        minPaint = new Paint();
        minPaint.setColor(Color.WHITE);
        minPaint.setTextSize(40);
        secPaint = new Paint();
        secPaint.setColor(Color.WHITE);
        secPaint.setTextSize(40);
        mHandler = new Handler();
    }

    private void parseTime(int time){
        int hr = time/3600;
        int min = 0;
        int sec = 0;
        if(hr>0){
            int offset = time - hr*3600;
            min = offset/60;
            sec = offset - min*60;
        } else {
            min = time/60;
            sec = time - min*60;
        }
        hrStr = hr + ":";
        minStr = (min<10 ? "0"+min : min) + ":";
        secStr = sec<10 ? "0"+sec : sec+"";
    }
}
