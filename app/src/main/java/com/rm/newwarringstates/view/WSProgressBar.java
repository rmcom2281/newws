package com.rm.newwarringstates.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.rm.newwarringstates.R;

/**
 * Created by User on 2018/1/7.
 */

public class WSProgressBar extends android.support.v7.widget.AppCompatImageView {
    private Paint textPaint;
    private String text;
    private Drawable backgroundBar, valueBar;
    private double widthRatio;

    public WSProgressBar(Context context) {
        super(context);
        initPaint(context);
    }

    public WSProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint(context);
    }

    public WSProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint(context);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Double width = getWidth()*widthRatio;
        backgroundBar.setBounds(0, 0, getWidth(), getHeight());
        backgroundBar.draw(canvas);
        valueBar.setBounds(0, 0, (width.intValue()/100), getHeight());
        valueBar.draw(canvas);
        Rect bounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        int x = getWidth() / 2 - bounds.centerX();
        int y = getHeight() / 2 - bounds.centerY();
        canvas.drawText(text, x, y, textPaint);
    }

    public synchronized void setText(Context context, int value, int maxValue) {
        this.widthRatio = value*100/maxValue;
        this.text = String.valueOf(value);
        if(widthRatio>90){
            valueBar = context.getResources().getDrawable(R.drawable.bg_title_bar_full_hightlight);
        } else {
            valueBar = context.getResources().getDrawable(R.drawable.bg_title_bar_hightlight);
        }
        drawableStateChanged();
    }

    private void initPaint(Context context){
        text = "0";
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(50);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        valueBar = context.getResources().getDrawable(R.drawable.bg_title_bar_hightlight);
        backgroundBar = context.getResources().getDrawable(R.drawable.bg_title_bar);
    }
}
